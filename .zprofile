#!/bin/zsh
# profile file. Runs on login. Environmental variables are set here.
# # Adds `~/.local/bin` to $PATH
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}" > /dev/null 2>&1

# unsetopt PROMPT_SP

# Default programs:
export EDITOR="nvim"

# # ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export KDEHOME="$XDG_CONFIG_HOME"/kde
export DVDCSS_CACHE="$XDG_CACHE_HOME"/dvdcss
export LESSHISTFILE="-"
#export LESS='-R --use-color -Dd+r$Du+b'
# export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export CONAN_USER_HOME="$XDG_CONFIG_HOME"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
#export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export FZF_DEFAULT_OPTS="--layout=reverse --cycle"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
