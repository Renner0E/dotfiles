#!/bin/zsh

# # Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}%n%{$fg[yellow]%}@%{$fg[blue]%}%m %{$fg[yellow]%}%~%{$reset_color%}$ %{$reset_color%}% "
#PS1="%B%{$fg[red]%}%n%{$fg[red]%}@%{$fg[red]%}%m %{$fg[red]%}%~%{$reset_color%}$ %{$reset_color%}% "
setopt autocd # Automatically cd into typed directory.
setopt histignoredups
setopt interactive_comments
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
# immediately write to history file
setopt INC_APPEND_HISTORY

# Completion order:
# 1st tabulation: complete as much as possible and show a list of options
# 2nd tabulation: complete with the 1st item in the list
# 3rd tabulation: complete with the 2nd item in the list, and so forth...
unsetopt list_ambiguous

# When the last character of a suggestion is a '/', typing a space right after
# will remove the '/'
setopt auto_remove_slash

# # Basic auto/tab complete:
zstyle ':completion:*' menu select
zmodload zsh/complist
comp_options+=(globdots) # Include hidden files.

# Autocomplete completions made for zsh
autoload -Uz +X compinit && compinit

# automatically load completions made for bash
autoload bashcompinit && bashcompinit

# fix fzf completion
#compdef _gnu_generic fzf
#compdef _gnu_generic ostree

# # vi mode
bindkey -v
export KEYTIMEOUT=1

# # Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# fix horrible flatpak names with autocomplete
# type firefox for org.mozilla.firefox need flatpaks in $PATH to work
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}' '+l:|=* r:|=*'

# # History in cache directory:
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=~/.config/zsh/history

# # Load aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"


# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp" >/dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
# ctrl + shortcuts
# Open lf in curent directory
bindkey -s '^o' 'lfcd\n'

# Copy last typed command not output
bindkey -s '^x' 'wl-copy !!\n'

bindkey '^[[P' delete-char

bindkey "\e[3~" delete-char

# Edit line in vim with ctrl-v:
# ctrl+e breaks dolphin konsole integration for whatever reason
autoload edit-command-line; zle -N edit-command-line
bindkey '^v' edit-command-line

# Load syntax highlighting autosuggestions and ctrl-r for history
# needs zsh zsh-syntax-highlighting zsh-autosuggestions fzf

# If brew is installed, source the stuff for brew
if [ -e /home/linuxbrew/.linuxbrew/bin/brew ]; then
  source /home/linuxbrew/.linuxbrew/opt/zsh-fast-syntax-highlighting/share/zsh-fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
  source /home/linuxbrew/.linuxbrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh

elif grep -q "^ID=arch" /etc/os-release; then
  source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
  source /usr/share/fzf/key-bindings.zsh

elif grep -q ^ID_LIKE=\""fedora\|bazzite\|rhel\|centos\"" /etc/os-release; then
  source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  source /usr/share/fzf/shell/key-bindings.zsh

elif grep -q "^ID=debian" /etc/os-release; then
  source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  source /usr/share/doc/fzf/examples/key-bindings.zsh
fi
