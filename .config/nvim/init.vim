if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'mbbill/undotree'
Plug 'tpope/vim-surround'
Plug 'jreybert/vimagit'
Plug 'tpope/vim-commentary'
Plug 'ap/vim-css-color'
Plug 'NoahTheDuke/vim-just'
call plug#end()

" set cyberpunk neon colorscheme
colorscheme cyberpunk
let g:airline_theme="cyberpunk"

" Drop vi compatibility
set nocompatible

" Force 256 colors support
set t_Co=256

set number relativenumber
set ruler
" set cursorline
set smartindent
set wildmode=longest,list,full
set ignorecase
set incsearch
set smartcase
set showmatch

" use Terminal background color for OLED
hi Normal guibg=NONE ctermbg=NONE

" Use U for undo instead of C-r
nnoremap U <C-r>

" center view when searching
nnoremap n nzz
nnoremap N Nzz
nnoremap * *zz
nnoremap # #zz
nnoremap g* g*zz
nnoremap g# g#zz

set undolevels=100 "maximum number of changes that can be undone
set undoreload=100 "maximum number lines to save for undo on a buffer reload

" No bells
set noerrorbells
set novisualbell

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=240 guibg=#585858
match ExtraWhitespace /\s\+$/
match ExtraWhitespace /\s\+$\| \+\ze\t/
match ExtraWhitespace /[^\t]\zs\t\+/

" Autoreload files
set autoread

set hlsearch
set showcmd
set fileformats=unix,dos,mac
set ttyfast
set encoding=utf-8
set mouse=a
filetype plugin on
syntax on
set title
" no tabs ever only spaces 2 wide
set shiftwidth=2
set tabstop=2
set expandtab
set history=1000
set clipboard+=unnamedplus
" Spell-check set to <leader>o, 'o' for 'orthography':
	map <leader>o :setlocal spell! spelllang=en_us<CR>
" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
	set splitbelow splitright

" Automatically deletes all trailing whitespace and newlines at end of file on save. & reset cursor position
  autocmd BufWritePre * let currPos = getpos(".")
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritePre * %s/\n\+\%$//e
	autocmd BufWritePre *.[ch] %s/\%$/\r/e
  autocmd BufWritePre * cal cursor(currPos[1], currPos[2])
if &diff
    highlight! link DiffText MatchParen
endif
