[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=cyberpunk2077
Font=Fira Code,12,-1,5,400,0,0,0,0,0,0,0,0,0,0,1
UseFontBrailleChararacters=false
UseFontLineChararacters=false

[Cursor Options]
CursorShape=1

[General]
Command=/bin/zsh -l
Environment=TERM=xterm-256color,COLORTERM=truecolor,LANGUAGE=en_US.UTF-8
InvertSelectionColors=true
Name=atomic-desktop
Parent=FALLBACK/
SemanticHints=1

[Interaction Options]
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=1
HistorySize=10000

[Terminal Features]
BlinkingCursorEnabled=true
LineNumbers=0
PeekPrimaryKeySequence=
UrlHintsModifiers=67108864
