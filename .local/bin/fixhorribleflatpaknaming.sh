#!/bin/bash
# Generates shell aliases for fucked up flatpak names :/

#set -ouex pipefail

# user or system flatpaks
flatpak_user_path="$HOME/.local/share/flatpak/app"
flatpak_system_path="/var/lib/flatpak/app"

# set this to change
flatpak_path=$flatpak_system_path
#flatpak_path="$flatpak_user_path"

for app in "$flatpak_path"/*; do
  if [ -d "$app" ]; then
    binary=$(grep "^command=" $app/current/active/metadata | sed 's/^command=//' | tr -d "\n")
    raw_list="alias $binary=\"flatpak run $(basename $app)\" "
    #echo $raw_list

    # filter binary names so they are not garbage like net.davidotek.pupgui2, torbrowser-launcher, yuzu-launcher, re.sonny.Eloquent
    # some of them will be garbage regardless like run.sh or something
    # remove -launcher/-wrapper/-run from ending
    # make them lowercase
    # if it still has a garbage name like re.sonny.Eloquent, only keep the last word
    # I totally know what's going on in the last one
    filtered_binary=$(echo $binary | sed \
      -e "s/-launcher$//" \
      -e "s/-run$//" \
      -e "s/-wrapper$//" \
      -e "s/.*/\L&/" \
      -Ee "s/.*\.[^.]*\.(.*)/\1/"
    )
    filtered_list="alias $filtered_binary=\"flatpak run $(basename $app)\" "
    echo $filtered_list
  fi
done
